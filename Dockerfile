ARG BUILD_FROM
FROM $BUILD_FROM
# Set Env
ENV LANG de_DE.UTF-8
ENV TERM xterm

# Install CUPS
RUN apk update --no-cache && apk add --no-cache cups cups-filters inotify-tools brlaser

# Copy configuration files
COPY root /

# Set workdir
WORKDIR /opt/cups



# Prepare CUPS container
RUN chmod 755 /srv/run.sh

# Start CUPS instance
CMD ["/srv/run.sh"]